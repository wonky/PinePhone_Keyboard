# SWMO Configs and Modified xkb files from Undef

The |\£€~-=_+ section of the top row can be accessed using AltG + top row keys. This is set in the xkb settings which were upstreamed by Undef. This was added in version xkeyboard-conf version > 2.35.1

XKB Source Information -  
https://gitlab.com/mobian1/packages/xkeyboard-config and,  
https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/merge_requests/322

Phosh (Mobian) Information -  
See the following: https://wiki.mobian-project.org/doku.php?id=ppaccessories

Phosh (Arch) - 
Update your `/usr/share/X11/xkb/*` to the files in https://codeberg.org/HazardChem/PinePhone_Keyboard/src/branch/main/xkb. If you don't want to change the files in /usr/share/xkb/ you can create them in your home directory, see [here](https://xkbcommon.org/doc/current/md_doc_user_configuration.html).

Add the folllowing in `~/.config/kxkbrc`

```
[Layout]
LayoutList=us
Model=ppkb
```

SWMO/Sway Information -  
Since 31/05/22 Sway has the config included to set the keyboard correctly. Leaving exisiting files for records.

General XKB Information -  
See the following for US layout issues, us(intl) works out of the box but the ' turns into an accent for letters:
https://wiki.archlinux.org/title/Xmodmap

Use the files located in rules and symbols directories to confirm that the ppkb settings for xkb are in your local files

Check the following locations:  
`/usr/share/X11/xkb/(rules or symbols)`  
Against the same named files in the directories that follow.

This can be checked by using the tool `diff` on the your local files against the above, using the script `ppkb_settings_test.sh`, or you can inspect the files by hand.